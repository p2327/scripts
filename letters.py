def letters_maker(start, finish):
    """
    An example of generator.
    It returns a letter within a specified range.
    
    >>> alphabet = letters_maker('a', 'z')
    >>> next(alphabet)
    'a'
    """
    while start <= finish:
        yield start
        start = chr(ord(start) + 1)
        
