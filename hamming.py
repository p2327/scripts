import typing

def distance(s: str, j: str) -> int:
    """
    Calculates the Hamming distance between two DNA strands.
    It takes two sequences of equal lenght and counts the differences.
    
    >>> distance('GAGCCTACTAACGGGAT', 'CATCGTAATGACGGCCT')
    7
    """

    if len(s) != len(j):
        raise ValueError('seq must be of same length')

    count = 0
    # zip returns tuples, unpacked and compared in a, b
    for a, b in zip(s, j):
        if a != b:
            count += 1
   
    return count