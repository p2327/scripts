from itertools import repeat
import typing
import re

def is_isogram(s: str) -> bool:
    """
    Tests if a word s is an isogram.
    An isogram is a word or phrase with no repeating letter.
    
    >>> isogram('background')
    True
    >>> isogram('isograms')
    False
    """
    # remove spaces and lowercase word
    s = re.sub(r'[^\w]', ' ', s)
    s = re.sub(r"\s+", "", s.lower())
    
    # createlist of 0s and hashtable for counts
    s_zeros = list(repeat(0, len(s)))
    s_hash = dict(zip(s, s_zeros))
    
    # count occurences
    for c in s:
        if c in s_hash.keys():
            s_hash[c] += 1
            
    if sum(s_hash.values()) > len(set(s)):
        return False
        
    return True